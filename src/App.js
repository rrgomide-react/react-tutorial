import React, { Component } from 'react';

import Tutorial1 from './components/Tutorial1';
import Tutorial2 from './components/Tutorial2';
import Tutorial3 from './components/Tutorial3';
import Tutorial4 from './components/Tutorial4';

class App extends Component {
  render() {
    return (
      <div>
        <h1>Tutorial de React - Raphael Gomide</h1>

        <Tutorial1 />
        <Tutorial2 />
        <Tutorial3 />
        <Tutorial4 />
      </div>
    );
  }
}

export default App;
