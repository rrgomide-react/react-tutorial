import React, { Component, Fragment } from 'react';
import LikeButton from './LikeButton';

export default class Tutorial2 extends Component {
  render() {
    return (
      <Fragment>
        <h2>
          Entendendo <code>state</code>
        </h2>

        <ul>
          <li>Clique diversas vezes no componente abaixo.</li>

          <ul>
            <li>
              A mudança de cor no componente é feita através do monitoramento do
              estado (<code>state</code>
              ).
            </li>
          </ul>
        </ul>

        <div style={{ textAlign: 'center' }}>
          <LikeButton />
        </div>

        <ul>
          <li>
            Estude o código-fonte do componente acima (
            <strong>LikeButton.js</strong>) e responda às seguintes questões:
          </li>

          <ul>
            <li>O que faz o botão "reagir"?</li>
            <li>Há alguma manipulação do DOM no código do botão?</li>
          </ul>
        </ul>

        <hr />
      </Fragment>
    );
  }
}
