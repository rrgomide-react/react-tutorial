import React, { Component } from 'react';

const styles = {
  row: {
    marginLeft: '20px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },

  label: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginRight: '10px'
  },

  input: {
    marginTop: '5px',
    width: '200px'
  }
};

export default class IMC extends Component {
  constructor() {
    super();

    this.state = {
      peso: 90.0,
      altura: 1.78,
      IMC: -1
    };
  }

  componentDidMount() {
    this.calcularIMC();
  }

  handleChange(campo, valor) {
    this.setState({ [campo]: +valor });
    this.calcularIMC();
  }

  calcularIMC() {
    try {
      const imc = this.state.peso / (this.state.altura * this.state.altura);

      /**
       * Verificando valores
       * inválidos
       */
      if (imc === Infinity || isNaN(imc)) {
        throw new Error('inválido');
      }

      this.setState({
        IMC: imc.toFixed(2)
      });
    } catch (error) {
      this.setState({
        IMC: error.message
      });
    }
  }

  render() {
    return (
      <div style={styles.row}>
        <label style={styles.label}>
          <span>Informe o seu peso (kg):</span>

          <input
            style={styles.input}
            type="number"
            min="0"
            max="200"
            step="0.1"
            value={this.state.peso}
            onChange={input => this.handleChange('peso', input.target.value)}
          />
        </label>

        <label style={styles.label}>
          <span>Informe a sua altura (m):</span>

          <input
            style={styles.input}
            type="number"
            min="1"
            max="2.5"
            step="0.01"
            value={this.state.altura}
            onChange={input => this.handleChange('altura', input.target.value)}
          />
        </label>

        <label style={styles.label}>
          <span>IMC:</span>

          <input
            style={styles.input}
            type="text"
            disabled={true}
            value={this.state.IMC}
          />
        </label>
      </div>
    );
  }
}
