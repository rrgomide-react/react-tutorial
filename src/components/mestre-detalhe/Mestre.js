import React, { Component } from 'react';
import Detalhe from './Detalhe';

export default class Mestre extends Component {
  constructor() {
    super();

    this.state = {
      novoPersonagem: '',

      nomes: [
        { id: 1, nome: 'Darth Vader' },
        { id: 2, nome: 'Luke Skywalker' },
        { id: 3, nome: 'Obi Wan Kenobi' },
        { id: 4, nome: 'R2D2' },
        { id: 5, nome: 'C3PO' }
      ]
    };

    this.incluirPersonagem = this.incluirPersonagem.bind(this);
    this.excluirPersonagem = this.excluirPersonagem.bind(this);
  }

  excluirPersonagem(id) {
    const nomesAtualizados = this.state.nomes.filter(nome => nome.id !== id);
    this.setState({ nomes: nomesAtualizados });
  }

  incluirPersonagem() {
    if (!this.state.novoPersonagem) {
      return;
    }

    const { nomes } = this.state;
    const ids = nomes.map(nome => nome.id);

    let id = nomes.length + 1;
    while (ids.some(item => item === id)) {
      id++;
    }

    nomes.push({ id, nome: this.state.novoPersonagem });
    this.setState({ nomes, novoPersonagem: '' });
  }

  render() {
    return (
      <div>
        <p>Total de personagens até o momento: {this.state.nomes.length}</p>

        <label>
          <span style={{ marginRight: '5px' }}>Novo personagem:</span>
          <input
            type="text"
            value={this.state.novoPersonagem}
            onChange={input =>
              this.setState({ novoPersonagem: input.target.value })
            }
            onKeyUp={input => {
              if (input.keyCode === 13) {
                this.incluirPersonagem();
              }
            }}
          />
        </label>

        <button onClick={this.incluirPersonagem}>Incluir</button>

        <Detalhe lista={this.state.nomes} onExcluir={this.excluirPersonagem} />
      </div>
    );
  }
}
