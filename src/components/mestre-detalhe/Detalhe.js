import React, { Component } from 'react';

export default class Detalhe extends Component {
  render() {
    if (!this.props.lista || this.props.lista.length === 0) {
      return <p>Nenhum personagem cadastrado</p>;
    }

    return (
      <table border="1" style={{ marginTop: '15px' }}>
        <thead>
          <tr>
            <th>Personagem</th>
            <th />
          </tr>
        </thead>

        <tbody>
          {this.props.lista.map(item => {
            return (
              <tr key={item.id}>
                <td>{item.nome}</td>
                <td>
                  <button onClick={() => this.props.onExcluir(item.id)}>
                    x
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}
