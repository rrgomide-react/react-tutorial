import React, { Component, Fragment } from 'react';

import Mestre from './mestre-detalhe/Mestre';

export default class Tutorial4 extends Component {
  render() {
    return (
      <Fragment>
        <h2>
          Comunicação entre componentes <code>pai</code> e <code>filho</code>{' '}
          através de <code>props</code>:
        </h2>

        <ul>
          <li>Manipule e estude o componente abaixo (arquivos):</li>
          <li>Responda às seguintes perguntas:</li>

          <ul>
            <li>
              O componente "Detalhe" possui <code>state</code>? Como ele é
              manipulado?
            </li>

            <li>
              Como foi feita a exclusão de itens? Quem faz a exclusão? Quem
              possui a interface de exclusão?
            </li>

            <li>O que faz o evento onKeyUp do input em "Mestre"?</li>
            <li>O que acontece se excluirmos todos os personagens?</li>
          </ul>
        </ul>

        <div>
          <Mestre />
        </div>

        <hr />
      </Fragment>
    );
  }
}
