import React, { Component, Fragment } from 'react';

const styles = {
  button: {
    border: '1px solid gray',
    padding: '5px',
    cursor: 'pointer',
    borderRadius: '10px',
    backgroundColor: 'transparent',
    width: '100px',
    height: '30px'
  },

  curtido: {
    color: '#e74c3c',
    backgroundColor: '#ecf0f1'
  },

  naoCurtido: {
    color: 'black'
  }
};

export default class LikeButton extends Component {
  constructor() {
    super();

    this.state = {
      curtido: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  getButton = () => {
    const text = this.state.curtido ? '♥ curtido' : '♥ curtir';
    const estado = this.state.curtido ? styles.curtido : styles.naoCurtido;
    const style = { ...styles.button, ...estado };

    return (
      <button style={style} onClick={this.handleClick}>
        {text}
      </button>
    );
  };

  handleClick = () => {
    this.setState({
      curtido: !this.state.curtido
    });
  };

  render() {
    return <Fragment>{this.getButton()}</Fragment>;
  }
}
