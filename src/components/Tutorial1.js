import React, { Component, Fragment } from 'react';

export default class Tutorial1 extends Component {
  render() {
    return (
      <Fragment>
        <ul>
          <li>React é um framework baseado em componentes.</li>
          <li>
            Cada component pode ter <code>state</code> e <code>props:</code>
            <ul>
              <li>
                <code>state</code> é o estado do componente, a parte dinâmica.
              </li>
              <li>
                <code>props</code> são propriedades do componente, bem
                semelhantes aos atributos de tags HTML.
              </li>
            </ul>
          </li>
        </ul>
        <hr />
      </Fragment>
    );
  }
}
