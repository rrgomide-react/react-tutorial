import React, { Component, Fragment } from 'react';

import IMC from './IMC';

export default class Tutorial3 extends Component {
  render() {
    return (
      <Fragment>
        <h2>
          Entendendo <code>state</code> (2).
        </h2>

        <ul>
          <li>
            Manipule e estude o componente abaixo (arquivo{' '}
            <strong>IMC.js</strong>
            ):
          </li>
        </ul>

        <div style={{ textAlign: 'center' }}>
          <IMC />
        </div>

        <ul>
          <li>Agora responda às seguintes perguntas:</li>

          <ul>
            <li>
              Os inputs de <strong>peso</strong> e <strong>altura</strong> são
              semelhantes. Foi feito algum tipo de refatoração para processar
              suas alterações? O que foi feito?
            </li>
            <li>
              Houve tratamento para <strong>casos extremos</strong>? O que foi
              feito?
            </li>

            <li>
              O estado (<code>state</code>) de IMC inicial é -1. O que foi feito
              para que o IMC fosse calculado inicialmente (com base em peso: 90
              e altura: 1.78) sem a intervenção do usuário?
            </li>
          </ul>
        </ul>

        <hr />
      </Fragment>
    );
  }
}
